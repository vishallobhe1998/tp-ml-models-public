import numpy as np
from pulp import *
from cds_player.models import Player


def one_lineup_type1(players,
                     previous_lineups,
                     batsmen,
                     bowlers,
                     all_rounders,
                     wicket_keepers,
                     num_overlap,
                     num_players,
                     bowler_opponent):
    model = LpProblem("Best Cricket Lineup", LpMaximize)
    # print(num_players)
    players_lineup = dict({i: LpVariable(cat=LpBinary,
                                         name="PLAYER_{}".format(i))
                           for i in range(num_players)})

    # 11 player constraint
    model += lpSum([players_lineup[i] for i in range(num_players)]) == 11

    # between 1 to 4 wicket keepers
    model += lpSum([players_lineup[i] * wicket_keepers[i] for i in range(num_players)]) <= 4
    model += lpSum([players_lineup[i] * wicket_keepers[i] for i in range(num_players)]) >= 1

    # between 3 to 6 batsmen
    model += lpSum([players_lineup[i] * batsmen[i] for i in range(num_players)]) >= 3
    model += lpSum([players_lineup[i] * batsmen[i] for i in range(num_players)]) <= 6

    # between 3 to 6 to bowlers
    model += lpSum([players_lineup[i] * bowlers[i] for i in range(num_players)]) >= 3
    model += lpSum([players_lineup[i] * bowlers[i] for i in range(num_players)]) <= 6

    # between 1 and 2 all rounders (can be more (between 1 and 4) but i have reduced it)
    # all rounders usually don't get chance to bat and bowl
    model += lpSum([players_lineup[i] * all_rounders[i] for i in range(num_players)]) >= 1
    model += lpSum([players_lineup[i] * all_rounders[i] for i in range(num_players)]) <= 2

    # financial constraint
    model += lpSum([players_lineup[i] * float(players[i, 7]) for i in range(num_players)]) <= 100

    # between 7 and 4 player from 1 team
    players_team_a = [0 for i in range(num_players)]
    players_team_b = [0 for i in range(num_players)]
    a = players[0, 4]
    for i in range(num_players):
        if (players[i, 4] == a):
            players_team_b[i] = 1
        else:
            players_team_a[i] = 1

    model += lpSum([players_lineup[i] * players_team_a[i] for i in range(num_players)]) <= 7
    model += lpSum([players_lineup[i] * players_team_a[i] for i in range(num_players)]) >= 4

    model += lpSum([players_lineup[i] * players_team_b[i] for i in range(num_players)]) <= 7
    model += lpSum([players_lineup[i] * players_team_b[i] for i in range(num_players)]) >= 4

    # no_batsmen_vs_bowler_combination
    for i in range(num_players):
        if bowlers[i] == 1:
            model += 6 * (players_lineup[i]) + lpSum(
                [bowler_opponent[k, i] * players_lineup[k] for k in range(num_players)]) <= 6

            # overlap constraint

    for i in range(len(previous_lineups)):
        model += lpSum([previous_lineups[i, j] * players_lineup[j] for j in range(num_players)]) <= num_overlap

    objective = lpSum([float(players[i, 6]) * players_lineup[i] for i in range(num_players)])

    # setting_objective

    model.setObjective(objective)

    status = model.solve()
    score = 0
    lineup_copy = [0 for i in range(num_players)]
    scores = []
    if status == 1:
        for i in range(num_players):
            if players_lineup[i].value() <= 1.1 and players_lineup[i].value() >= 0.9:
                lineup_copy[i] = 1
                scores.append([float(players[i, 6]), i])

    scores = sorted(scores)

    # 2 and 3 are cap and vc they are the ones with maximum scores
    lineup_copy[scores[-1][1]] = 2
    lineup_copy[scores[-2][1]] = 3

    for i in range(len(scores) - 2):
        score += scores[i][0]
    score += scores[-1][0] * 2
    score += scores[-2][0] * 1.5

    lineup_copy = np.array(lineup_copy)
    return lineup_copy, score


def modify(lineup):
    ans = []
    for i in range(len(lineup)):
        if lineup[i] > 0:
            ans.append(1)
        else:
            ans.append(0)
    ans = np.array(ans)
    return ans


def get_lineups(match_id,
                num_lineups,
                num_overlap,
                players,
                ):
    '''
    # num_lineups is an integer that is the number of lineups
    # num_overlap is an integer that gives the overlap between each lineup
    # path_players is a string that gives the path to the players csv file
    # path_to_output is a string where the final csv file with your lineups will be
    '''
    num_players = players.shape[0]
    print(players)
    # create_batsmen
    batsmen = []
    bowlers = []
    wicket_keepers = []
    all_rounders = []

    for i in range(num_players):
        if players[i, 2] == "BAT":
            batsmen.append(1)
            bowlers.append(0)
            wicket_keepers.append(0)
            all_rounders.append(0)
        elif players[i, 2] == "BOWL":
            batsmen.append(0)
            bowlers.append(1)
            wicket_keepers.append(0)
            all_rounders.append(0)
        elif players[i, 2] == "AR":
            batsmen.append(0)
            bowlers.append(0)
            wicket_keepers.append(0)
            all_rounders.append(1)
        elif players[i, 2] == "WK":
            batsmen.append(1)
            bowlers.append(0)
            wicket_keepers.append(1)
            all_rounders.append(0)

    batsmen = np.array(batsmen)
    bowlers = np.array(bowlers)
    wicket_keepers = np.array(wicket_keepers)
    all_rounders = np.array(all_rounders)

    # create bowlers opponenets
    bowler_opponent = np.zeros((num_players, num_players))
    for i in range(num_players):
        if bowlers[i] == 1:
            opp = players[i, 4]
            for j in range(num_players):
                if (bowlers[j] != 1):
                    team = players[j, 3]
                    if team == opp:
                        bowler_opponent[i, j] = 1

    previous_lineups = np.zeros((2, num_players))
    lineup1, score1 = one_lineup_type1(players,
                                       previous_lineups,
                                       batsmen,
                                       bowlers,
                                       all_rounders,
                                       wicket_keepers,
                                       num_overlap,
                                       num_players,
                                       bowler_opponent)

    modified_lineup1 = modify(lineup1).reshape((1, num_players))
    x = np.zeros((1, num_players))
    previous_lineups = np.concatenate((modified_lineup1, x), axis=0)
    lineup2, score2 = one_lineup_type1(players,
                                       previous_lineups,
                                       batsmen,
                                       bowlers,
                                       all_rounders,
                                       wicket_keepers,
                                       num_overlap,
                                       num_players,
                                       bowler_opponent)

    modified_lineup2 = modify(lineup2).reshape((1, num_players))
    previous_lineups = np.concatenate((modified_lineup1, modified_lineup2), axis=0)

    lineup1 = lineup1.reshape((1, num_players))
    lineup2 = lineup2.reshape((1, num_players))
    lineups = np.concatenate((lineup1, lineup2), axis=0)
    scores = []
    scores.append(score1)
    scores.append(score2)

    for i in range(num_lineups - 2):
        print("creating_lineup_{}".format(i + 3))
        lineupi, scorei = one_lineup_type1(players,
                                           previous_lineups,
                                           batsmen,
                                           bowlers,
                                           all_rounders,
                                           wicket_keepers,
                                           num_overlap,
                                           num_players,
                                           bowler_opponent)
        modified_lineupi = modify(lineupi).reshape((1, num_players))
        lineupi = lineupi.reshape((1, num_players))

        lineups = np.concatenate((lineups, lineupi), axis=0)
        scores.append(scorei)
        previous_lineups = np.concatenate((previous_lineups, modified_lineupi), axis=0)

    lineups_dict = {}
    for i in range(num_lineups):
        lineups_dict[i] = {'lineup': [], "predicted-score": round(scores[i], 2)}
        lineup_i = lineups[i]
        for j in range(num_players):
            if lineup_i[j] > 0:
                lineups_dict[i]['lineup'].append({
                    'player_id': players[j, 0],
                    'isViceCaptain': True if lineup_i[j] == 2 else False,
                    'isCaptain': True if lineup_i[j] == 3 else False,
                    'type': players[j, 2],
                    'team': players[j, 3],
                })

    return lineups_dict
