"""
all the logic to get prediction for new match.
"""
from src.result_save import save_fantasy_team


def save_predictions(match_id, lineups, player_data):
    """
    save the prediction to server.
    """
    result = []
    # save fantasy team lineups
    for team, i in enumerate(lineups):
        player_lineups = lineups[team]['lineup']
        players = []
        for item in player_lineups:
            players.append({
                "credits": list(filter(
                    lambda x: x[0] == item['player_id'],
                    player_data
                ))[0][-1],
                "points": list(filter(
                    lambda x: x[0] == item['player_id'],
                    player_data
                ))[0][-2],
                "player": item['player_id'],
                "type": item['type'],
                "team": item['team'],
                "isViceCaptain": item['isViceCaptain'],
                "isCaptain": item['isCaptain'],
            })
        result.append(
            save_fantasy_team(
                name=str(i + 1),
                points=sum(map(lambda x: int(x['points']), players)),
                match=match_id,
                provider=1,
                players=players
            )
        )

    return result
