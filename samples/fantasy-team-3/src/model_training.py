"""
all the logic to define and train predictive model
"""
from cds_scorecard.models import MatchPlayers
from cds_match_inning.models import MatchBowlersInning, MatchBatsmanInning
from cds_match.models import Match
import numpy as np


def calculate_score_per_match(
        batting_inning: dict,
        bowling_inning: dict,
        matchtype: str
):
    '''
    calculate score from player stats using this formula
    score = [
     Innings Runs Scored Num +  Innings Boundary Sixes+
     Innings Boundary Fours+ 2*Innings Boundary Sixes+
     4*50's + 8*"100's + 25*Innings Wickets Taken + 4*4 Wickets+
     8*5 Wickets + 4*Innings Maidens Bowled
     ]
    '''

    if not batting_inning:
        batting_inning = []
    if not bowling_inning:
        bowling_inning = []

    score = 0

    points = []
    if matchtype == 'odi':
        points = [1, 2, 1, 4, 8, 25, 4, 8, 4]
    elif matchtype == 't20':
        points = [1, 2, 1, 8, 16, 25, 8, 16, 8]
    else:
        points = [1, 2, 1, 4, 8, 16, 4, 8, 0]

    for inning in batting_inning:
        runs = inning.runs
        half_century = 1 if runs >= 50 and runs < 100 else 0
        century = 1 if runs >= 100 else 0
        score = points[0] * runs + \
                points[1] * inning.sixes + \
                points[2] * inning.fours + \
                points[3] * half_century + \
                points[4] * century

    for inning in bowling_inning:
        wickets = inning.wickets
        four_wickets = 1 if wickets >= 4 and wickets < 5 else 0
        five_wickets = 1 if wickets >= 5 else 0

        score = points[5] * wickets + \
                points[6] * four_wickets + \
                points[7] * five_wickets + \
                points[8] * inning.maidens

    return score


def convert_ground(player_features):
    """
    average of  all score of same ground
    replace with ground

    """
    grounds = np.unique(player_features[:, [0]])
    mapping = {}
    for ground in grounds:
        sameground_features = player_features[player_features[:, 0] == ground]
        mapping[ground] = np.sum(sameground_features[:, 1])
        newarray = np.asarray(sameground_features)
        newarray[:, 0] = np.repeat(mapping[ground], sameground_features.shape[0])
        player_features[player_features[:, 0] == ground] = newarray

    return mapping, player_features


def training_player(
        player_id: int,
        league_id: int,
):
    '''
    training data prepared here
    which is used in prediction of score
    Return:
    ----------
    csv file with column:
    score , is_home_ground , venue_score
    opp_team_score , consistency , batting_order
    opp_team_rank
    '''

    # all matches of one league in which  player played
    match_ids = MatchPlayers.objects.filter(
        player__id=player_id,
        match__series__league_id=league_id
    ).values_list('match_id').distinct()

    # now calculate score , venue wise score of every match
    # earlier to current match
    player_features = []
    for match_id in match_ids:
        match_id = match_id[0]
        match = Match.objects.get(id=match_id)

        batting_inning = MatchBatsmanInning.objects.filter(
            player_id=player_id,
            scorecard__match_id=match_id
        )

        bowling_inning = MatchBowlersInning.objects.filter(
            player_id=player_id,
            scorecard__match_id=match_id
        )
        # score calculation
        score = calculate_score_per_match(
            batting_inning=batting_inning,
            bowling_inning=bowling_inning,
            matchtype=match.matchtype
        )

        player_features.append([
            match.venue.id,
            score,
        ])
    player_features = np.array(player_features, dtype='float32')

    if player_features.shape[0] == 0:
        return np.array([]), np.array([]), np.array([]), np.array([])

    # venue_id replace with score of earlier match of same venueid
    mapping, player_features = convert_ground(player_features)

    X = player_features[:, 0].reshape(-1, 1)
    Y = player_features[:, 1].reshape(-1, 1)

    return player_features[0], mapping, X, Y
