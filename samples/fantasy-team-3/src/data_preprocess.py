"""
functions to pre-process data after fetching the data from server.
"""
from cds_match.models import Match, MatchSquad
from cds_provider.models import ProviderPlayerCredit
from cds_team.models import Team
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F
from sklearn.linear_model import SGDRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
import numpy as np
from src.model_training import training_player


def player_team(
        player_id: int
):
    '''
    return team in which player belong
    '''
    return Team.objects.filter(
        players__id=player_id
    ).distinct()


def player_opposition_team(
        player_id: int,
        match_id: int
):
    '''

    return  player opp team in
     a match

    '''

    try:
        match = Match.objects.get(id=match_id)
    except ObjectDoesNotExist:
        return None

    player_team_ = player_team(player_id)[0]

    player_opp_team = match.team1 \
        if player_team_ == match.team1 else match.team2

    return player_opp_team



def preprocess(match_id):
    '''

    training data prepared here
    which is used in prediction of fantasyteam
    Return:
    ----------
    csv file with column:
    player name , Team , type ,
    opponent , credits , Ground,
    Is_Home_Team , projection

    '''

    match = Match.objects.get(id=match_id)

    match_players = MatchSquad.objects.filter(
        match_id=match.id
    ).annotate(player_name=F('player__name'))


    if not match_players or len(match_players) < 22:
        return []

    output_for_lineup = []
    for player in match_players:


        #earlier matches data of player used as training data for prediction
        df , dict_ground , X_train , y_train = training_player(
            player_id=player.id,
            league_id=match.series.league.id,
        )
        #oppostion team and team of palyer
        player_opp_team = player_opposition_team(player.player_id, match_id)
        player_team = match.team2 \
            if player_opp_team == match.team1 else match.team1

        score_pred = 0
        if X_train.shape[0] != 0:
            #prediction of score
            best_mlp = make_pipeline(StandardScaler(),
                                     SGDRegressor(max_iter=10000, tol=1e-5))
            best_mlp.fit(X_train, y_train)

            score_prediction = best_mlp.predict(
                np.array([
                        dict_ground.get(match.venue.id,1),
                ],dtype=float).reshape(-1,1)
            )

            score_pred = round(score_prediction[0],2)
            if score_pred < 0:
                score_pred = 1

        output_for_lineup.append([
             player.player_id,
             player.player_name,
             player.type,
             player_team.id,
             player_opp_team.id,
             match.venue.id,
             score_pred,
             ProviderPlayerCredit.objects.get(
                 player_id=player.player_id).cost
           ])
    array = np.array(output_for_lineup)
    return array
