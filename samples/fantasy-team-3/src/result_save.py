"""
all the logic to save prediction on
"""
import requests
from src.utlis import API_URL, API_TOKEN
import json


def save_fantasy_team(
        name, points, match, provider, players
):
    """
    save playing 11 prediction.
    {
    "name": "9",
    "points": 320.0,
    "match": 2903,
    "provider": 1,
    "players": [
            {
                "credits": 12,
                "points": 23,
                "type": "WK",
                "isCaptain": false,
                "isViceCaptain": false,
                "team": 32,
                "player": 46
            }
        ]
    }
    """
    url = ""
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    payload = {

    }
    response = requests.post(url, headers=headers, json=payload)
    return response.json()
