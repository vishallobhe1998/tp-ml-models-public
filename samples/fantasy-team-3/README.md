# ML Model Lambda Template
Template of creating and deploying ML Model for Trusted Prediction.

## Prerequisites
1. Create your account at [trustedprediction.com](https://www.trustedpredictions.com)  
   ```
   import requests
   url = "https://api.sportsanalytics.in/users/"
    payload={"username": "", "password": "","email": "","refer_user_token": ""}
    headers = {
      'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    print(response.text)

   ```
   Copy the ID in response and rename your project to playing11-(Your User ID)
   or winner-(Your User ID) or fantasy-team-(Your User ID) or simulation-(Your User ID)  
<br>
2. Create ML Model  

   ```
   import requests
   url = "https://api.sportsanalytics.in/admin/algorithm/ml-model/"
   payload={'name': '', 'description': '', 'author': (Your User ID)}
   files=[]
   response = requests.request("POST", url, headers=headers, data=payload, files=files)
   print(response.text)

   ```
   Copy the ID (ML Model ID). This will be your ml model id.
<br>
3. Create Virtual Environment and install python dependencies.
   ```
   python3 -m venv venv
   source activate venv/bin/activate
   pip install -r requirements.txt
   python src/lambda_function

   ```