from cds_match.models import MatchSquad
from cds_match_inning.models import MatchBatsmanInning, MatchFieldingInning, MatchBowlersInning
from cds_scorecard.models import MatchPlayers
from django.db.models import Sum, Q

API_URL = "https://api.trustedpredictions.com"
ML_MODEL_ID = 4

