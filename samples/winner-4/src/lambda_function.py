# configure django
import json

import django
from django.conf import settings

settings.configure(
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'cricket',
            'USER': 'ml_intern',
            'PASSWORD': 'Password@123',
            'HOST': 'trusted-predictions-production-read.csgvze83fbfc.ap-south-1.rds.amazonaws.com',
            'PORT': '5432',
        }
    },
    INSTALLED_APPS=[
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'cds_algorithm',
        'cds_chat',
        'cds_coin_manager',
        'cds_commentary',
        'cds_competitors',
        'cds_contact',
        'cds_continent',
        'cds_countries',
        'cds_espn',
        'cds_fantasy_teams',
        'cds_league',
        'cds_marketplace',
        'cds_match',
        'cds_match_inning',
        'cds_money_manager',
        'cds_news',
        'cds_payment',
        'cds_player',
        'cds_playerstats',
        'cds_prediction',
        'cds_provider',
        'cds_scorecard',
        'cds_series',
        'cds_sport',
        'cds_team',
        'cds_team_vs_team',
        'cds_teamstats',
        'cds_tour',
        'cds_umpire',
        'cds_user_chat',
        'cds_venue',
        'cds_weather',
        'cds_website',
        'user',
    ]
)
django.setup()

# from src.data_collection import collect_data
from src.data_preprocess import preprocess
from src.model_training import get_model
from src.model_prediction import save_predictions
from cds_match.models import Match


def lambda_handler(event, context):
    # get the match id
    match_id = event['match_id']

    # get match data
    match = Match.objects.get(
        id=match_id
    )

    # # # get the data
    # data = collect_data()

    # # pre process all collected data
    train_X, train_y, valid_X = preprocess(match)

    # # get lineups for playing 11

    model = get_model(train_X, train_y)

    # # save the prediction to server
    prediction = save_predictions(model, valid_X, match)

    response = {
        "statusCode": 200,
        "body": json.dumps(prediction)
    }

    return response


if __name__ == '__main__':
    lambda_handler({"match_id": 2985}, {})
