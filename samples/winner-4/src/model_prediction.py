"""
all the logic to get prediction for new match.
"""
from src.result_save import save_winner


def save_predictions(model, valid_X, match):
    """
    save the prediction to server.
    """
    match_id = match.id
    predictions_prob = model.predict_proba(valid_X)[0]
    predictions = model.predict(valid_X)
    if len(predictions_prob) == 1:
        return [0, 0, 0]
    elif len(predictions_prob) == 2:
        prediction = {
            'percentage_winning_team1': predictions_prob[0],
            'percentage_winning_team2': predictions_prob[1],
            'winner': predictions,
        }
    else:
        prediction = {
            'percentage_winning_team1': predictions_prob[1],
            'percentage_winning_team2': predictions_prob[2],
            'winner': predictions,
        }

    prediction['match'] = match_id
    # prediction['team_strength'] = valid_X[:, 3]
    prediction['is_drawn'] = True if prediction['winner'] == 0 else False
    prediction['winner'], prediction['winning_percentage'] = \
        (match.team1.id, prediction['percentage_winning_team1']) \
            if prediction['winner'] == 1 else \
            (match.team2.id, prediction['percentage_winning_team2']) \
                if prediction['winner'] == 2 else None
    prediction['team1_predicted_score'] = prediction['percentage_winning_team1']
    prediction['team2_predicted_score'] = prediction['percentage_winning_team2']
    prediction.pop('percentage_winning_team1')
    prediction.pop('percentage_winning_team2')
    prediction.pop('winning_percentage')

    return save_winner(**prediction)
