"""
functions to pre-process data after fetching the data from server.
"""

import numpy as np
import pandas as pd
from cds_prediction.models import PredictionMatch

from src.model_training import modeling_team


def training_dataset_preparation(row):
    '''
    1. function to convert winner team id
    into 0-draw or abandoned, 1-team1 win ,2-team2 win
    2. remove the nan , inf , -inf
    '''
    if row[0] == row[1]: row[0] = 1
    elif row[0] == row[2]: row[0] = 2
    # else: row[0] = 0
    # row[np.isneginf(row)] = 0
    # row = np.nan_to_num(row)
    return row


def preprocess(match):
    """
    pre-process the fetched data.
    """
    # for prediction fetch all matches of that league (trainning data)
    predmatches = PredictionMatch.objects.filter(
        match__status='complete',
        match__start_date__lt=match.start_date,
        match__match_detail__winner__isnull=False,
        match__series__league_id=match.series.league.id
    ).values_list('match__match_detail__winner',
                  'match__team1', 'match__team2',
                  'match__venue__id', 'team_strength')

    df = pd.DataFrame.from_records(
        predmatches
        , columns=['winner', 'team1', 'team2',
                   'venue', 'strength']
    )
    df = df.dropna()
    predmatches = df.values

    # df.replace([np.inf, -np.inf], np.nan, inplace=True)
    # df = df.fillna(0)
    # df.to_csv('dataset_5.csv',index=False)

    # convert queryset into numpy
    predmatches = np.array(predmatches, dtype='float32')
    predmatches = np.apply_along_axis(training_dataset_preparation, 1, predmatches)

    # split the trainning data
    train_X = predmatches[:, [1, 2, 3, 4]]
    train_y = predmatches[:, 0]

    # for prediction of current match need inputdata
    valid_X = np.array([[
        match.team1.id,
        match.team2.id,
        match.venue.id,
        round(modeling_team(match.id), 3)
    ]], dtype='float32')

    return [train_X, train_y, valid_X]
