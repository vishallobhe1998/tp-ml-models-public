"""
all the logic to define and train predictive model
"""
import numpy as np
from cds_match.models import Match, MatchSquad
from cds_match_inning.models import MatchFieldingInning, MatchBatsmanInning, MatchBowlersInning
from cds_scorecard.models import MatchPlayers
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum, Q
from sklearn.ensemble import GradientBoostingClassifier

def get_player_stats(
        match,
        player
):
    '''
    player batting stats,bowlingstats,last 5 match run
    '''
    player_matches = MatchPlayers.objects.filter(
        player=player,
        match__series__league=match.series.league,
        match__start_date__lte=match.start_date,
    ).order_by('-match__start_date').values_list('match_id', flat=True)

    player_batting_innings = MatchBatsmanInning.objects.filter(
        player=player,
        scorecard__match__series__league=match.series.league,
        scorecard__match__start_date__lte=match.start_date,
        scorecard__match__in=player_matches.values_list('match_id', flat=True)
    ).order_by('-scorecard__match__start_date')
    not_out_innings = player_batting_innings.filter(fall_of_wicket_over=0, how_out="not out")
    out_innings = player_batting_innings.count() - not_out_innings.count()
    total_runs = player_batting_innings.aggregate(Sum('runs'))['runs__sum']

    fours = player_batting_innings.aggregate(Sum('fours'))['fours__sum']
    sixes = player_batting_innings.aggregate(Sum('sixes'))['sixes__sum']

    fours = fours if fours else 0.0
    sixes = sixes if sixes else 0.0

    # batting stats
    batting_stats = {
        'innings': player_batting_innings.count(),
        'matches': player_matches.count(),
        'centuries': player_batting_innings.filter(runs__gte=100).count(),
        'half_centuries': player_batting_innings.filter(Q(runs__gte=50) & Q(runs__lt=100)).count(),
        'fours': fours,
        'sixes': sixes,
        'thirties': player_batting_innings.filter(Q(runs__gte=30) & Q(runs__lt=50)).count(),
        'average': round(total_runs / out_innings,
                         2) if out_innings != 0 and out_innings and total_runs else total_runs,
    }

    # bowling stats
    player_bowling_innings = MatchBowlersInning.objects.filter(
        player=player,
        scorecard__match__series__league=match.series.league,
        scorecard__match__start_date__lte=match.start_date,
        scorecard__match__in=player_matches.values_list('match_id', flat=True)
    )
    total_runs = player_bowling_innings.aggregate(Sum('run_conceded'))['run_conceded__sum']
    total_overs = player_bowling_innings.aggregate(Sum('overs'))['overs__sum']
    total_wickets = player_bowling_innings.aggregate(Sum('wickets'))['wickets__sum']

    total_wickets = total_wickets if total_wickets else 0

    bowling_stats = {
        'innings':player_bowling_innings.count(),
        'matches': player_matches.count(),
        'wickets':total_wickets,
        'wicket_2': player_bowling_innings.filter(wickets__gte=2).count(),
        'wicket_4': player_bowling_innings.filter(wickets__gte=4).count(),
        'average': round(total_runs / total_wickets, 2) if total_wickets != 0 and
                                                           total_runs and total_wickets else 0,
        'economy': round(total_runs / total_overs, 2) if total_overs != 0
                                                         and total_runs and total_overs else 0
    }
    #last 5 match runs
    last5matches_runs = MatchBatsmanInning.objects.filter(
        player=player,
        scorecard__match__series__league=match.series.league,
        scorecard__match__start_date__lte=match.start_date,
        scorecard__match__in=player_matches[:5]
    ).aggregate(Sum('runs'))['runs__sum']

    #fielding inning
    player_fielding_innings = MatchFieldingInning.objects.filter(
        player=player,
        scorecard__match__series__league=match.series.league,
        scorecard__match__start_date__lte=match.start_date,
        scorecard__match__in=player_matches.values_list('match_id', flat=True)
    )

    fielding_stats = {
        'stump':player_fielding_innings.filter(isStump=True).count(),
        'catch':player_fielding_innings.filter(isCaught=True).count(),
        'run_out':player_fielding_innings.filter(isRunOut=True).count()
    }

    return batting_stats , bowling_stats , last5matches_runs , fielding_stats


def match_players(
    match,
    team_id:int,
    prediction:bool=True,
):
    '''
    return players from match players model if playing 11 present if not
    then return from squad table
    '''

    players = MatchSquad.objects.filter(
        match_id=match.id,
        team_id=team_id
    ).values_list('player', flat=True).distinct()
    return players


def modeling_players(
        match_id: int,
        team_id: int,
):
    '''
    Calculate batting score and bowling score of all player
    playing in current match with team.id==team_id

    batting_score:
    ________________
        for all players p ∈ {P (A, m) ∪ P (B, m)} do
        φ ← q φ(p)
        u ← sqrt(φ(bat_inning)/φ(matches played))
        v ← 20 ∗ φ N um Centuries + 5 ∗ φ N um
        w ← 0.3 ∗ v + 0.7 ∗ φ Bat Avg
        φ Career Score ← u ∗ w
        M ← Last 4 matches played by p
        φ Recent Score ← mean(M Runs)
        φ Batsmen Score <- 0.35 ∗φ CareerScore+ 0.65 ∗ φ RecentScore

    bowling_score:
    ________________
        u ← sqrt(φ(bat_inning)/φ(matches played))
        v ← 10 ∗ φFWkts Hauls + φWkts
        w ← φ Bowl Avg ∗ φ Bowl Eco
        φ Bowler Score ← u∗v/w
    '''

    match = Match.objects.get(id=match_id)

    players = match_players(
        match=match,
        team_id=team_id,
    )
    career_score_list, recent_score_list = [], []
    bowling_score_list, batting_score_list = [], []

    for player in players:

        batting_stats, bowling_stats, last5matches_runs, _ = get_player_stats(match, player)
        # carrer score calculation
        u = np.sqrt(
            np.divide(batting_stats['innings'],
                      batting_stats['matches'])
        ) if batting_stats['matches'] != 0 else 0

        v = np.sum([
            np.multiply(15, batting_stats['centuries']),
            np.multiply(5, batting_stats['half_centuries']),
            np.multiply(5, batting_stats['thirties'])
        ])

        w = np.add(
            np.multiply(0.3, v),
            np.multiply(0.7, batting_stats['average'])
        ) if batting_stats['average'] \
            else np.add(np.multiply(0.3, v), 0)

        career_score = np.multiply(u, w)

        # last 5 match runs for recent score calculation
        recent_score = np.divide(
            last5matches_runs,
            5
        ) if last5matches_runs else 0

        career_score_list.append(career_score)
        recent_score_list.append(recent_score)

        # bowlingscore
        if bowling_stats:
            u = np.sqrt(np.divide(
                bowling_stats['innings'],
                bowling_stats['matches'])
            ) if bowling_stats['matches'] != 0 else 0

            v = np.sum([
                np.multiply(6, bowling_stats['wicket_2'])
                if bowling_stats['wicket_2'] else 0,

                np.multiply(4, bowling_stats['wicket_4'])
                if bowling_stats['wicket_4'] else 0,

                bowling_stats['wickets']
                if bowling_stats['wickets'] else 0
            ])

            w = np.multiply(
                bowling_stats['average'],
                bowling_stats['economy']
            )

            bowling_score = np.divide(
                np.multiply(u, v),
                w
            ) if w != 0 else 0

            bowling_score_list.append(bowling_score)

    career_score_list = [np.divide(x, max(career_score_list))
                         for x in career_score_list]

    recent_score_list = [np.divide(x, max(recent_score_list))
                         for x in recent_score_list]

    bowling_score_list = [np.divide(x, max(bowling_score_list))
                          for x in bowling_score_list]

    # batsmen strenth
    for c, r in zip(career_score_list, recent_score_list):
        batting_score_list.append(
            np.multiply(0.35, c) + np.multiply(0.65, r)
        )

    batting_score_list = [np.divide(
        x, max(batting_score_list)
    ) for x in batting_score_list]

    return np.sum(batting_score_list), \
           np.sum(bowling_score_list)


def modeling_team(
        match_id,
        pred: bool = False
):
    '''

     relative strength of team A against team B

    '''

    try:
        match = Match.objects.get(id=match_id)
    except ObjectDoesNotExist:
        return None

    # calculating team strength of batting and bowling of both team
    team1_bat_strength, team1_bowl_strength = modeling_players(match_id, match.team1.id)
    team2_bat_strength, team2_bowl_strength = modeling_players(match_id, match.team2.id)

    # relative strength of both team
    team_strength = np.subtract(
        np.divide(team1_bat_strength, team2_bowl_strength),
        np.divide(team2_bat_strength, team1_bowl_strength)
    ) if team2_bowl_strength != 0.0 and team1_bowl_strength != 0.0 else 0

    return team_strength


def get_model(train_X, train_y):
    """
    train the model with data and return the model.
    """
    model = GradientBoostingClassifier()
    model.fit(train_X, train_y)

    return model
