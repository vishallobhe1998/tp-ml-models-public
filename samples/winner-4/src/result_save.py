"""
all the logic to save prediction on
"""
import requests
from src.utlis import API_URL, ML_MODEL_ID


def save_winner(
        match, winner, team1_predicted_score, team2_predicted_score,
        is_drawn=False, is_abandoned=False, toss_winner=None
):

    url = API_URL + "/admin/algorithm/winner/"

    payload = {
        "is_abandoned": is_abandoned,
        "is_drawn": is_drawn,
        "team1_predicted_score": team1_predicted_score,
        "team2_predicted_score": team2_predicted_score,
        "match": match,
        "toss_winner": toss_winner,
        "winner": winner,
        "ml_model": ML_MODEL_ID,
    }
    response = requests.request("POST", url, data=payload)
    return response.json()
