"""
all the logic to get prediction for new match.
"""
from src.result_save import save_playing11


def save_predictions(lineups, match):
    """
    save the prediction to server.
    """
    result = []
    # save playing 11 lineups
    for i in range(len(lineups)):
        team_lineup = lineups[i]
        for item in team_lineup.T.to_dict().values():
            result.append(
                save_playing11(
                    player=item['player_id'],
                    predicted_score=item['index'],
                    match=match['id'],
                    team=match['squads'][i]['team']['id'],
                    type=list(filter(
                        lambda x: x['player_id'] == item['player_id'],
                        match['squads'][i]['players']
                    ))[0]['type'],
                    order=None
                )
            )

    return result
