"""
all the logic to define and train predictive model
"""
import numpy as np
import pandas as pd
from pulp import *


def lineup_mip(players,
               is_bowler,
               is_batsman,
               is_wk,
               is_ar):
    l = np.concatenate((np.eye(4), np.array([1, 1, 1, 0]).reshape((1, 4))), axis=0)
    p = np.zeros(4)
    if (is_bowler[-1] == 1):
        p[0] = 1
    if (is_batsman[-1] == 1):
        p[1] = 1
    if (is_wk[-1] == 1):
        p[2] = 1
    if (is_ar[-1] == 1):
        for j in range(3):
            p[j] = 1

    num_players = len(players) - 1

    model = LpProblem("Best Cricket Lineup", LpMaximize)

    Q = dict({i: LpVariable(cat=LpBinary,
                            name="PLAYER_{}".format(i))
              for i in range(num_players)})

    c = is_batsman
    e = is_bowler
    f = is_wk
    g = is_ar
    S = players.perf_index.values

    model += lpSum([Q[i] for i in range(num_players)]) == 10

    model += lpSum([Q[i] * c[i] for i in range(num_players)]) >= 3 - np.sum(l[1] * p)

    model += lpSum([Q[i] * f[i] for i in range(num_players)]) >= 1 - np.sum(l[2] * p)

    model += lpSum([Q[i] * g[i] for i in range(num_players)]) >= 1 - np.sum(l[3] * p)

    model += lpSum([Q[i] * e[i] for i in range(num_players)]) >= 3 - np.sum(l[0] * p)

    objective = lpSum([Q[i] * S[i] for i in range(num_players)])

    model.setObjective(objective)

    status = model.solve()

    lineup_copy = [0 for i in range(num_players)]

    if status == 1:
        for i in range(num_players):
            if (Q[i].value() == 1):
                lineup_copy[i] = 1
    return lineup_copy


def create_lineup(df_input):
    df_players = df_input
    is_bowler = np.array([0 for i in range(len(df_players))])
    is_batsman = np.array([0 for i in range(len(df_players))])
    is_wk = np.array([0 for i in range(len(df_players))])
    is_ar = np.array([0 for i in range(len(df_players))])

    player_types = df_players.type.values

    for i in range(len(player_types)):
        if ("bowler" in player_types[i].strip()):
            is_bowler[i] = 1
        if ("batsmen" in player_types[i].strip()):
            is_batsman[i] = 1
        if ("wicketkeeper" in player_types[i].strip()):
            is_wk[i] = 1
        if ("all-rounder" in player_types[i].strip()):
            is_ar[i] = 1

    lineup = lineup_mip(df_players,
                        is_bowler,
                        is_batsman,
                        is_wk,
                        is_ar)
    lineup_names = []
    for i in range(len(lineup)):
        if (lineup[i] == 1):
            lineup_names.append(df_players.at[i, "player_id"])
    lineup_names.append(df_players.at[df_players.index[-1], "player_id"])

    df_final = pd.DataFrame(lineup_names, columns=["player_id"])
    if len(df_final) == 11:
        return df_final
    else:
        sort = df_players.sort_values(by=['perf_index'], ascending=False)
        result_df = pd.DataFrame(sort.head(11)['player_id'],columns=['player_id']).reset_index()
        return result_df


def get_lineups(data):
    """
    train the model with data and return the model.
    """
    lineups = []
    for team_data in data:
        lineups.append(create_lineup(team_data))

    return lineups
