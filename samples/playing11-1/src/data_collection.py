"""
functions to fetch data from postgresql server
"""
import psycopg2


def fetch_data(query):
    """
    fetch data from trusted prediction postgresql server.
    """
    # establishing the connection
    conn = psycopg2.connect(
        database="cricket", user='postgres', password='Sumit@123', host='sportsanalytics.in', port='5432'
    )

    # Setting auto commit false
    conn.autocommit = True

    # Creating a cursor object using the cursor() method
    cursor = conn.cursor()

    # Retrieving data
    cursor.execute(query)

    # Fetching rows from the table
    result = cursor.fetchall()

    # Commit your changes in the database
    conn.commit()

    # Closing the connection
    conn.close()

    return result


def collect_data():
    """
    collect all the data you need from server.
    """
    data = fetch_data("select * from cds_match_match limit 10")

    return data


if __name__ == '__main__':
    print(collect_data())
