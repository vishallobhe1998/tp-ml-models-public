"""
all the logic to save prediction on
"""
import requests
from src.utlis import API_URL, ML_MODEL_ID


def save_playing11(
        match, team, player, order, type, predicted_score,
        is_confirmed=False, is_captain=False, is_vice_captain=False,
):
    """
    save playing 11 prediction.
    :param match:
    :param team:
    :param player:
    :param is_confirmed:
    :param order:
    :param type:
    :param is_captain:
    :param is_vice_captain:
    :param predicted_score:
    :param ml_model:
    :return:
    """
    url = API_URL + "/admin/algorithm/playing11/"
    payload = {
        'match': match,
        'team': team,
        'player': player,
        'is_confirmed': is_confirmed,
        'order': order,
        'type': type,
        'is_captain': is_captain,
        'is_vice_captain': is_vice_captain,
        'predicted_score': predicted_score,
        'ml_model': ML_MODEL_ID,
    }
    response = requests.request("POST", url, data=payload)
    return response.json()
