"""
functions to pre-process data after fetching the data from server.
"""

import numpy as np
import pandas as pd
from cds_league.models import League
from cds_match_inning.models import MatchBatsmanInning, MatchBowlersInning, MatchFieldingInning
from cds_player.models import Player
from cds_scorecard.models import MatchPlayers
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from lifelines import KaplanMeierFitter


def calculate_bp(
        player_ids: list,
        league: object,
        num_matches: int = None
):
    # batting innings scorecard
    player_batting_innings = MatchBatsmanInning.objects.filter(
        scorecard__match__series__league=league,
        player__in=player_ids
    )[:num_matches].values('runs', 'balls', 'how_out', 'scorecard__runs', 'scorecard__over', 'player')

    df = pd.DataFrame(player_batting_innings).rename(
        columns={'how_out': 'out', 'scorecard__runs': 'match_runs', 'scorecard__over': 'match_balls'}
    )

    df['out'] = df['out'].apply(lambda x: 0 if x == 'not out' else 1)
    df['match_balls'] = df['match_balls'].apply(lambda x: int(x * 6))

    df['strike_rate'] = df['runs'] / df['balls']
    df['match_strike_rate'] = df['match_runs'] / df['match_balls']

    df['adjusted_run'] = df['runs'] * ((df['strike_rate'] / df['match_strike_rate']) ** 0.5)

    df = df.fillna(0)

    data = pd.DataFrame(index=player_ids, columns=['BP', 'S1'])
    for player_id in player_ids:
        # create a kmf object
        try:
            kmf = KaplanMeierFitter()

            # Fit the data into the model
            kmf.fit(df[df['player'] == player_id]['adjusted_run'].values, df[df['player'] == player_id]['out'].values,
                    label='Kaplan Meier Estimate')

            # Kaplan-Meier estimate of the mean adjusted runs scored by the i th batsman
            data.loc[player_id, "BP"] = kmf.predict(df[df['player'] == player_id]['adjusted_run'].mean())
        except Exception:
            data.loc[player_id, "BP"] = 0

    # calculate batting performance index
    data['S1'] = data['BP'] / data['BP'].mean()
    return data


def calculate_wkp(
        player_ids: list,
        league: object,
        num_matches: int = None
):
    data = pd.DataFrame(index=player_ids, columns=['DR', 'BR', 'S3'])
    for player_id in player_ids:
        try:
            player = Player.objects.get(pk=player_id)
        except ObjectDoesNotExist:
            continue

        # dismissal and bye runs
        player_matches = MatchPlayers.objects.filter(
            player=player
        ).values_list('match', flat=True).distinct()[:num_matches]

        player_wk_innings = MatchBatsmanInning.objects.filter(
            scorecard__match__series__league=league,
            scorecard__match__in=player_matches
        ).values_list('scorecard__bye').distinct()

        df = pd.DataFrame.from_records(
            player_wk_innings,
            columns=['bye'],
        )
        num_innings = player_matches.count()
        bye_runs = df['bye'].sum()

        total_dismissal = MatchFieldingInning.objects.filter(
            Q(isStump=True) | Q(isCaught=True),
            player_id=player_id
        ).count()

        data.loc[player_id, "DR"] = total_dismissal / num_innings if num_innings != 0 else 0
        data.loc[player_id, "BR"] = num_innings / bye_runs if bye_runs != 0 else 0

    data["DR"] = data["DR"] / data["DR"].mean()
    data["BR"] = (data["BR"]) / (data["BR"]).mean()

    D = np.squeeze(np.array(data["DR"]))
    B = np.squeeze(np.array(data["BR"]))

    S1 = D.std()
    S2 = B.std()
    alpha = S1 / S2
    threshold = 0.001
    while ((S1 - S2) >= threshold):
        B = np.power(B, alpha)
        S2 = B.std()
        alpha = S1 / S2

    beta_array = np.arange(0.5, 0.95, 0.05)
    Ranks = {}
    for beta in beta_array:
        WK = np.power(D, beta) * np.power(np.power(B, alpha), (1 - beta))
        temp = (-WK).argsort()
        ranks = np.empty_like(temp)
        ranks[temp] = np.arange(len(WK))
        ranks = ranks + 1
        Ranks[beta] = ranks
    Distance = {}
    for beta_j in beta_array:
        d = []
        for beta_p in beta_array:
            if beta_j != beta_p:
                d.append(np.power(Ranks[beta_j] - Ranks[beta_p], 2))
        Sum = np.sum(d) / 8
        Distance[beta_j] = Sum
    beta = min(Distance, key=Distance.get)

    data["WK"] = np.power(D, beta) * np.power(np.power(B, alpha), (1 - beta))
    data["S3"] = data["WK"] / data["WK"].mean()
    return data


def calculate_bop(
        player_ids: list,
        league: object,
        num_matches: int = None
):
    player_bowling_innings = MatchBowlersInning.objects.filter(
        scorecard__match__series__league=league,
        player__in=player_ids
    )[:num_matches].values('wickets', 'overs', 'run_conceded', 'player')

    weights_dict = {0: 0, 1: 1.30, 2: 1.35, 3: 1.40, 4: 1.45, 5: 1.38, 6: 1.18, 7: 0.98, 8: 0.79, 9: 0.59, 10: 0.39,
                    11: 0.19}

    data = pd.DataFrame(index=player_ids, columns=['CBR', 'S2'])
    df = pd.DataFrame(player_bowling_innings)
    df['Weights'] = 0

    for x in player_ids:
        order = MatchBatsmanInning.objects.filter(
            scorecard__match__series__league=league,
            player_id=x
        ).values_list('order', flat=True)

        weights = []
        for w in order:
            for key in weights_dict.keys():
                if w == key:
                    weights.append(w * weights_dict[key])

        sum = np.sum(np.array(weights))
        df.loc[x, "Weights"] = sum

    for player_id in player_ids:
        player_data = df[df['player'] == player_id]
        weights = player_data['Weights'].sum()
        balls = int(player_data['overs'].sum() * 6)
        runs = player_data['run_conceded'].sum()
        data.loc[player_id, "CBR"] = (3 * runs) / (weights + (balls / 6) + (weights * runs / balls))

    data["S2"] = (1 / data["CBR"]) / np.mean(1 / data["CBR"])
    return data


def calculate_performance_index(
        player_ids: list,
        league_id: int,
        num_matches: int = None
):
    league = League.objects.get(pk=league_id)
    players = Player.objects.filter(pk__in=player_ids).distinct()

    data = pd.DataFrame(index=players.values_list("id", flat=True), columns=['S1', 'S2', 'S3', 'type', 'player_id'])

    data['S1'] = calculate_bp(player_ids, league, num_matches)['S1']
    data['S2'] = calculate_bop(player_ids, league, num_matches)['S2']
    data['S3'] = calculate_wkp(player_ids, league, num_matches)['S3']
    data["a"] = [1 if player.type == "BOWL" else 0 for player in players]
    data["L"] = 0

    for id in player_ids:
        player = Player.objects.get(pk=id)
        data.loc[id, "type"] = player.type
        data.loc[id, "player_id"] = player.id
        if player.type == "BOWL" or player.type == "WK":
            data.loc[id, "L"] = data.loc[id, "S2"] ** data.loc[id, "a"] + data.loc[id, "S3"] ** (
                    1 - data.loc[id, "a"]) - 1

    C = ((1 / data['S1'].std()) + (1 / data['L'].std())) ** -1
    w1 = C / data['S1'].std()
    w2 = C / data['L'].std()
    data['perf_index'] = w1 * data['S1'] + w2 * data['S2']
    data = data.fillna(0)
    result_df = data[['player_id', 'type', 'perf_index']]
    result_df.index = range(len(result_df))
    return result_df


def preprocess(match):
    """
    pre-process the fetched data.
    """
    player_performance_index = []
    for team in match['squads']:
        player_performance_index.append(
            calculate_performance_index(
                player_ids=list(map(lambda x: x['player_id'], team['players'])),
                league_id=match['card']['league_id'],
            )
        )

    return player_performance_index
