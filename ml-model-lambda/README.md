# ML Model Lambda Template
Template of creating and deploying ML Model for Trusted Prediction.

## Development steps
1. Create your account at [trustedprediction.com](https://www.trustedpredictions.com)  
   ```
    import requests
    
    url = "https://api.trustedpredictions.com/auth/login/"
    
    payload={"username": "", "password": ""}
    headers = {
      'Content-Type': 'application/json',
    }
    
    response = requests.request("POST", url, headers=headers, data=payload)
    
    print(response.text)

    ```
   Copy the pk (User Id) in response and rename your project to playing11-(Your User ID)
   or winner-(Your User ID) or fantasy-team-(Your User ID) or simulation-(Your User ID)  
   Copy the API Token (key) and paste in utils as API_TOKEN
   
   If you do not have account, please create your account on trustedpredictions.com
   

2. Create ML Model  

   ```
   import requests
   url = "https://api.trustedpredictions.com/admin/algorithm/ml-model/"
   payload={'name': '', 'description': '', 'author': (Your User ID)}
   files=[]
   response = requests.request("POST", url, headers=headers, data=payload, files=files)
   print(response.text)

   ```
   Copy the ID (ML Model ID). This will be your ml model id.
<br>
3. For creating the models, we are gonna use the 'src' folder exclusively.
   a) "data_collection.py" should hold the logic of fetching the data from database.
   b) "data_preprocess.py" should hold the logic of preprocessing the data in accordance to 
      the model's requirements
   c) "model_training.py" should hold the model training / optimization code.
   d) "model_prediction.py" should have the code for making the predictions out of trained model.
   e) "result_save.py" contain a bunch of functions which help saving your predictions from the
      above file to our databases(this file should not be altered).
   f) "lambda_function.py" file is the driver file of this folder and connects all the files mentioned
      above.

   In addition, you can add any file in the src folder as you please.
<br>
4. To deploy the model, first define your python dependencies in local_requirements.txt
<br>
5. Create Virtual Environment and install python dependencies.
   ```
   python3 -m venv venv
   source activate venv/bin/activate
   pip install -r local_requirements.txt
   python src/lambda_function

   ```