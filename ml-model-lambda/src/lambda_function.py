import psycopg2

from src.data_collection import collect_data
from src.data_preprocess import preprocess
from src.model_training import get_trained_model
from src.model_prediction import save_predictions
from src.utils import ROLLBAR_TOKEN
import rollbar

rollbar.init(ROLLBAR_TOKEN , 'development')


def main(event, context):
    # get the match id
    match_id = event['match_id']

    # get the data
    data = collect_data()

    # pre process all collected data
    data = preprocess(data)

    # get trained all model
    model = get_trained_model(data)

    # save the prediction to server
    predictions = save_predictions(model)

    response = {
        "statusCode": 200,
        "body": predictions
    }

    return response


def lambda_handler(*args, **kwargs):
    response = {}
    try:
        response = main(*args, **kwargs)
    except IOError:
        rollbar.report_message('Got an IOError in the main loop', 'warning')
    except:
        # catch-all
        rollbar.report_exc_info()
        # equivalent to rollbar.report_exc_info(sys.exc_info())

    return response


if __name__ == '__main__':
    print(lambda_handler({"match_id": 2985}, {}))
