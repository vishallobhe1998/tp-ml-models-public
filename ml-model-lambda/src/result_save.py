"""
all the logic to save prediction on
"""
"""
all the logic to save prediction on
"""
import requests
from src.utlis import API_URL, ML_MODEL_ID, API_TOKEN


def save_playing11(
        match, team, player, order, type, predicted_score,
        is_confirmed=False, is_captain=False, is_vice_captain=False,
):
    """
    save playing 11 prediction.
    :param match:
    :param team:
    :param player:
    :param is_confirmed:
    :param order:
    :param type:
    :param is_captain:
    :param is_vice_captain:
    :param predicted_score:
    :param ml_model:
    :return:
    """
    url = API_URL + "/admin/algorithm/playing11/"
    payload = {
        'match': match,
        'team': team,
        'player': player,
        'is_confirmed': is_confirmed,
        'order': order,
        'type': type,
        'is_captain': is_captain,
        'is_vice_captain': is_vice_captain,
        'predicted_score': predicted_score,
        'ml_model': ML_MODEL_ID,
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()


def save_winner(
        match, toss_winner, winner, team1_predicted_score, team2_predicted_score,
        text = None, is_abandoned = False, is_drawn = False
) :
    
    """
    save winner.
    :param match:
    :param is_abandoned:
    :param is_drawn:
    :param toss_winner:
    :param winner:
    :param team1_predicted_score:
    :param team2_predicted_score:
    :param ml_model:
    :param text:
    :return:
    """
    url = API_URL + '/admin/algorithm/winner/'
    payload = {
        'match': match,
        'is_abandoned': is_abandoned,
        'is_drawn ': is_drawn,
        'toss_winner': toss_winner,
        'winner': winner,
        'team1_predicted_score': team1_predicted_score,
        'team2_predicted_score': team2_predicted_score,
        'ml_model': ML_MODEL_ID,
        'text': text,
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()


def save_fantasy_team(
        match, name, predicted_points, actual_points, highest_points, players,
        provider, ml_model, text = None
) :
    
    """
    save fantasy team.
    :param match:
    :param name:
    :param predicted_points:
    :param actual_points:
    :param highest_points:
    :param players:
    :param provider:
    :param ml_model:
    :param text:
    :return:
    """
    url = API_URL + '/admin/algorithm/fantasy-team/'
    payload = {
        'match': match,
        'name': name,
        'predicted_points': predicted_points,
        'actual_points': actual_points,
        'highest_points': highest_points,
        'players': players,
        'provider': provider,
        'ml_model': ML_MODEL_ID,
        'text': text,
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()


def save_scorecard(
        innings, wickets, no_ball, wide, runs, required_run_rate, over, run_rate,
        leg_bye, bye, extra, team, opp_team, match, is_declared = False, 
) :
    
    """
    save scorecard.
    :param innings:
    :param wickets:
    :param no_ball:
    :param wide:
    :param runs:
    :param required_run_rate:
    :param over:
    :param run_rate:
    :param leg_bye:
    :param bye:
    :param extra:
    :param team:
    :param opp_team:
    :param match:
    :param is_declared:
    'param ml_model'
    :return:
    """
    url = API_URL + '/admin/algorithm/scorecard/'
    payload = {
        'innings': innings,
        'wickets': wickets,
        'no_ball': no_ball,
        'wide': wide,
        'runs': runs,
        'required_run_rate': required_run_rate,
        'over': over,
        'run_rate': run_rate,
        'leg_bye': leg_bye,
        'bye': bye,
        'extra': extra,
        'team': team,
        'opp_team': opp_team,
        'match': match,
        'is_declared': is_declared,
        'ml_model' : ML_MODEL_ID
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()


def save_fantasy_player(
        id, predicted_points, actual_points, type, match, player, team,
        text = None, is_captain = False, is_vice_captain = False
) :
    
    """
    save fantasy team player.
    :param id:
    :param predicted_points:
    :param actual_points:
    :param type:
    :param match:
    :param player:
    :param team:
    :param text:
    :param is_captain:
    :param is_vice_captain:
    :param ml_model:
    :return:
    """
    url = API_URL + '/admin/algorithm/fantasy-team-player/'
    payload = {
        'id': id,
        'predicted_points': predicted_points,
        'actual_points': actual_points,
        'type': type,
        'match': match,
        'player': player,
        'team': team,
        'text': text,
        'is_captain': is_captain,
        'is_vice_captain': is_vice_captain,
        'ml_model' : ML_MODEL_ID
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()


def save_batsman_inning(
        order, run, balls, strike_rate, four, sixes, how_out, fall_of_wicket_over,
        scorecard_prediction, player
) :
    
    """
    save batsman inning.
    :param order:
    :param run:
    :param balls:
    :param strike_rate:
    :param four:
    :param sixes:
    :param how_out:
    :param fall_of_wicket_over:
    :param scorecard_prediction:
    :param player:
    :param ml_model:
    :return:
    """
    url = API_URL + '/admin/algorithm/batsman-inning/'
    payload = {
        'order' : order,
        'run' : run,
        'balls': balls,
        'strike_rate': strike_rate,
        'four': four,
        'sixes': sixes,
        'how_out': how_out,
        'fall_of_wicket_over': fall_of_wicket_over,
        'scorecard_prediction': scorecard_prediction,
        'player': player,
        'ml_model' : ML_MODEL_ID
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()


def save_bowler_inning(
        order, runs_conceded, maidens, wickets, overs, economy, wides, no_balls, fours, sixes,
        zeros, scorecard_prediction, player
) :
    
    """
    save bowler inning.
    :param order:
    :param runs_conceded:
    :param maidens:
    :param wickets:
    :param overs:
    :param economy:
    :param wides:
    :param no_balls:
    :param fours:
    :param sixes:
    :param zeros:
    :param scorecard_prediction:
    :param player:
    :param ml_model:
    :return:
    """
    url = API_URL + '/admin/algorithm/bowler-inning/'
    payload = {
        'order': order,
        'runs_conceded': runs_conceded,
        'maidens': maidens,
        'wickets': wickets,
        'overs': overs,
        'economy': economy,
        'wides': wides,
        'no_balls': no_balls,
        'fours': fours,
        'sixes': sixes,
        'zeros': zeros,
        'scorecard_prediction': scorecard_prediction,
        'player': player,
        'ml_model' : ML_MODEL_ID
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()

def save_fielding_inning(
        bowler, fielder, scorecard_prediction, player, is_out = False,
        is_caught = False, is_bowled = False, is_lbw = False, is_runout = False
) :
    
    """
    save fielding inning.
    :param bowler:
    :param fielder:
    :param scorecard_prediction:
    :param player:
    :param is_out:
    :param is_caught:
    :param is_bowled:
    :param is_lbw:
    :param is_runout:
    :param ml_model:
    :return:
    """

    url = API_URL + '/admin/algorithm/fielding-inning/'
    payload = {
        'bowler': bowler,
        'fielder': fielder,
        'scorecard_prediction': scorecard_prediction,
        'player': player,
        'isOut': is_out,
        'isCaught': is_caught,
        'isLbw': is_lbw,
        'isRunOut': is_runout,
        'ml_model' : ML_MODEL_ID
    }
    headers = {
        'Authorization': 'Token ' + API_TOKEN,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    try:
        response = requests.post(url, headers=headers, json=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)

    return response.json()